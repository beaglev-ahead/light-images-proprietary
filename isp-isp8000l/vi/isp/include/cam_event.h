#ifndef __CAM_EVENT_H__
#define __CAM_EVENT_H__

#include "list.h"
#include "stdint.h"
#include "csi_camera.h"
#include "pthread.h"

#define CAM_EVENT_MAX_PER_TYPE  (32)
#define CAM_EVENT_PAYLOAD_SIZE (128)
#define CAM_CHECK_CSI_EVENT_PAYLOAD_SIZE(size)\
    char test_array[sizeof(((csi_camera_event_s *)0)->bin)==CAM_EVENT_PAYLOAD_SIZE?1:-1]
typedef enum camera_event_type{
    CAM_EVENT_TYPE_CAM =0,
    CAM_EVENT_TYPE_CHANNEL = CAM_EVENT_TYPE_CAM+CAM_EVENT_MAX_PER_TYPE,
    CAM_EVENT_TYPE_INTERNAL = CAM_EVENT_TYPE_CHANNEL+ CAM_EVENT_MAX_PER_TYPE,

}camera_event_type_e;
// typedef enum cam_event_id{

//      /*  match the define order in csi_camera_event_id_e */
//     CAM_EVENT_WRN = CAM_EVENT_TYPE_CAM,
//     CAM_EVENT_ERR,
//     CAM_EVENT_SENSOR_FIRST_IMAGE_ARRIVE,
//     CAM_EVENT_ISP_3A_ADJUST_READY,
//     CAM_EVENT_MAX ,

//  /*  match the define order in csi_camera_channel_event_id_e */

//     CAM_CHANNEL_EVENT_FRAME_DONE = CAM_EVENT_TYPE_CHANNEL,
//     CAM_CHANNEL_EVENT_FRAME_PUT,
//     CAM_CHANNEL_EVENT_OVERFLOW,
//     CAM_CHANNEL_EVENT_MAX ,
//     CAM_INTERNEL_EVENT_FRAME_DONE = CAM_EVENT_TYPE_INTERNEL,
//     CAM_INTERNEL_EVENT_FRAME_DONE_RY,
//     CAM_INTERNEL_EVENT_MAX,
// }cam_event_id_e;

// #define  GET_EVENT_CAM(ev)      ((ev-CAM_EVENT_TYPE_CAM)&(CAM_EVENT_MAX_PER_TYPE-1))
// #define  GET_EVENT_CHANNEL(ev)    ((ev-CAM_EVENT_TYPE_CHANNEL)&(CAM_EVENT_MAX_PER_TYPE-1))
// #define  GET_EVENT_INTERNEL(ev)  ((ev-CAM_EVENT_TYPE_INTERNEL)&(CAM_EVENT_MAX_PER_TYPE-1))

typedef enum cam_subdev_event_id{
    CAM_EVENT_ERR =0,
    CAM_PIPLINE_ERR,
    CAM_SUBDEV_EVENT_ERR,
    CAM_SUBDEV_EVENT_WRN,
    CAM_SUBDEV_EVENT_SENSOR_FIRST_IMAGE_ARRIVE,
    CAM_SUBDEV_EVENT_FRAME_PUT,
    CAM_SUBDEV_EVENT_OVERFLOW,
    CAM_SUBDEV_EVENT_FRAME_DONE,
    CAM_SUBDEV_EVENT_INVALID,
}cam_subdev_event_id_e;

typedef struct cam_event_map{
    cam_subdev_event_id_e event_table[32];
}cam_event_map_t;
/***************************************cam_event field define******************************/
/************ ******|[31：24]8 bit MASK CAM_EVENT_TYPE|[23:16]8 bit channle id/Path id|[15:8]8 bit subdev id|[7:0]8 bit event id|**********************/

typedef unsigned int cam_event_wrap;

#define  CAM_EVENT_PACK_WRAP(ev)      ((CAM_EVENT_TYPE_CAM<<24)|(ev&0xf))
#define  CHANNEL_EVENT_PACK_WRAP(ev)  ((CAM_EVENT_TYPE_CHANNEL<<24)|(ev&0xf))
#define  INTERNAL_EVENT_PACK_WRAP(ev) ((CAM_EVENT_TYPE_INTERNAL<<24)|(ev&0xf))


/******************************************************************************/
#define  WRAP_ADD_CHANNEL_ID(ev,ch)     (ev|((ch&0xff)<<16))
#define  WRAP_ADD_SUBDEV_ID(ev,dev)     (ev|((dev&0xff)<<8))
#define  WRAP_ADD_PATH_ID(ev,path)     (ev|((path&0xff)<<16))
#define WRAP_UNPACK_EVENT_TYPE(ev)  (ev>>24&0xff)
#define WRAP_UNPACK_CHANNEL_ID(ev)  (ev>>16&0xff)
#define WRAP_UNPACK_SUBDEV_ID(ev)   (ev>>8&0xff)
#define WRAP_UNPACK_PATN_ID(ev)     (ev>>16&0xff)
#define WRAP_UNPACK_EVENT_ID(ev)    (ev&0x1f)
/****************************************************************************************/
typedef enum csi_cam_internal_event{
    CSI_CAM_INTERNEL_EVENT_ERR = (0x1),
    CSI_CAM_INTERNEL_EVENT_WRN = (0x1<<1),
    CSI_CAM_INTERNEL_EVENT_FRAME_DONE = (0x1<<2),
}csi_cam_internal_event_t;

typedef struct csi_cam_cond {
	pthread_mutex_t mutex;
	pthread_cond_t cond;
} csi_cam_cond_t;

typedef struct cam_event_queue_item{

    // csi_camera_event_s event;
    cam_event_wrap event_wrap;
    // event_wrap_t event_wrap;
    struct timespec		timestamp;
    union{
         char data[CAM_EVENT_PAYLOAD_SIZE];
    };
    struct cam_event_queue_item *next;
}cam_event_queue_item_t;


typedef struct event_switch{
    unsigned int on_evt;
    cam_event_map_t  map;
}event_switch_t;

typedef struct cam_event_queue{
    struct event_queue{
         cam_event_queue_item_t *head;
         cam_event_queue_item_t *tail;
         int exit;
    }event_queue;
    csi_cam_cond_t cond;
}cam_event_queue_t;

typedef struct csi_cam_head{
    camera_event_type_e type;
    void* parent_ctx;
}csi_cam_head_t;
typedef struct csi_cam_event_ctx{

     csi_cam_head_t head;
     unsigned int on_cam_ev;   //bitmamsk the reigster Cam event
     cam_event_queue_t cam_event_queue;

    //  event_switch_t cam_evt;
    //  unsigned int on_internal_ev;   //bitmamsk the reigster Cam event
    //  event_switch_t cam_internal_evt;
     cam_event_queue_t internel_queue;
     pthread_t inter_thread;
     csi_cam_cond_t cond;
    //  struct event_queue{
    //      cam_event_queue_item_t *head;
    //      cam_event_queue_item_t *tail;
    //      int exit;
    //  }event_queue;
     void* cam_handler;
     void **ch_ev_ctx_list;
     unsigned int ch_ev_num;
    void **inter_ev_ctx_list;
     unsigned int inter_ev_num;
}csi_cam_event_ctx_t;

typedef struct csi_pipline_event_ctx{
    csi_cam_head_t head;
    int pipe_id;
    unsigned int on_event;  //bitmamsk the reigster channle event
    event_switch_t channel_evt;
    cam_event_queue_t  *queue;
    // csi_cam_event_ctx_t *cam_ptr;

}csi_pipeline_event_ctx_t;


typedef struct csi_internal_event_ctx{
    csi_cam_head_t head;
    int subdev_id;    // index in  cam_device_t:subdevs[]
    int path_id;     // index in  cam_subdev_t:subdev_path[]
    unsigned int on_event;  //bitmamsk the reigster internal event
    event_switch_t channel_evt;
    cam_event_queue_t  *queue;

}csi_internal_event_ctx_t;


 typedef union cam_event_ctx_wrapper{
            csi_cam_head_t head;
            csi_pipeline_event_ctx_t pipeline_ctx;
            csi_cam_event_ctx_t  cam_ctx;
            csi_internal_event_ctx_t subdev_ctx;
}cam_event_ctx_wrapper_t;



csi_cam_event_ctx_t * camera_create_event(void* cam);

void camera_destroy_event(csi_cam_event_ctx_t * ctx);
void cam_event_queue_init(cam_event_queue_t *ctx);
void cam_event_destroy(csi_cam_event_ctx_t *ctx);
void cam_event_set(csi_cam_event_ctx_t *ctx,int mask);
void cam_event_clean(csi_cam_event_ctx_t *ctx,int mask);
int cam_event_get(csi_cam_event_ctx_t *ctx,int mask);
int cam_event_cahnnel_set(csi_cam_event_ctx_t *ctx,int id,int mask);
int cam_event_cahnnel_clean(csi_cam_event_ctx_t *ctx,int id,int mask);
void cam_event_internal_set(csi_internal_event_ctx_t *ctx,int mask);

void cam_event_internal_clean(csi_internal_event_ctx_t *ctx,int mask);
int cam_event_internal_get(csi_internal_event_ctx_t *ctx,int mask);
void cam_event_callback(void *context, cam_subdev_event_id_e event, void *arg,size_t size);
cam_event_queue_item_t * cam_wait_event(cam_event_queue_t *ctx);
unsigned int convert_to_csi_event_type(cam_event_wrap evnet);
unsigned int convert_to_csi_event_id(cam_event_wrap evnet);
csi_pipeline_event_ctx_t * camera_create_channel_event(csi_cam_event_ctx_t * ctx,int ch_id);
void camera_destroy_channel_event(csi_cam_event_ctx_t * ctx,csi_pipeline_event_ctx_t * ch_ctx);
csi_internal_event_ctx_t* camera_create_inter_event(csi_cam_event_ctx_t * ctx,int subdev_id,int path_id);
void camera_destroy_inter_event(csi_internal_event_ctx_t* ctx);
int subdev_event_link_to_cam_event(unsigned int cam_evt,cam_subdev_event_id_e id);
int subdev_event_link_to_channle_event(unsigned int channel_evt,cam_subdev_event_id_e id);
int subdev_event_link_to_inter_evt(unsigned int inter_evt,cam_subdev_event_id_e id);
int camera_link_with_ch_event(csi_internal_event_ctx_t* inter_ctx,csi_pipeline_event_ctx_t *ch_ctx);

void cam_send_err_event(csi_cam_event_ctx_t *event_ctx, void *data,size_t size);
void cam_send_wrn_event(csi_cam_event_ctx_t *event_ctx, void *data,size_t size);
#endif
