/*
 * @Description: 
 */
/*
 * Copyright (c) 2021 Alibaba Group. All rights reserved.
 * License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#ifndef _SUBDEV_SHARED_DEFS_
#define _SUBDEV_SHARED_DEFS_

#include <stdint.h>
#include "csi_frame_ex.h"
#include "csi_camera.h"

#define MAX_PLANES  3
typedef struct line_entry_path {
	uint32_t id;
	uint16_t entry_line_num;
	uint32_t buf_line_num;
	uint32_t width;
	uint32_t height;
	uint32_t stride;
	uint32_t buf_size;/**/
	csi_pixel_fmt_e data_format;
	uint64_t buf_addr;
	uint32_t bayer_mode;
    unsigned long frame_mark;
} line_entry_path_t;

typedef void (*release_frame_cb)(void* owner);
typedef struct cam_frame_img {
    csi_img_type_e type;
	uint32_t	width;
	uint32_t	height;
    uint32_t    fmt;
	uint32_t	num_planes;
	union {
        cam_frame_dmabuf_t dmabuf[MAX_PLANES];
		void  *phy_addr[MAX_PLANES];	// strores in phy contigous memory(s)
		void  *usr_addr[MAX_PLANES];    //stores in usr contigous memory(s)
	};
	uint32_t strides[MAX_PLANES];
    void *modifier;
    release_frame_cb release;
	uint8_t channel_id;
	int instID; // instance number represent a video channel in multi-thread scenario.
} cam_frame_img_t;

typedef struct cam_frame_priv{
    void *modifier;
    release_frame_cb release;
}cam_frame_priv_t;
typedef void (*cb_fn)(void*context,void*arg);
typedef struct cam_dsp_algo_para {

    char* algo_name;
    struct{
            cb_fn  cb;
            void*  context;
            int    arg_size;
    }algo_cb;
    void* sett_ptr;
    size_t sett_size;
    int    extra_buf_num;
    int    *extra_buf_sizes;
    void   **extra_bufs;

} cam_dsp_algo_para_t;

typedef struct cam_dsp_algo_buf {
    void* src_buf;
    void* replace_buf;
}cam_dsp_algo_buf_t;

typedef struct cam_pp_param {
    uint16_t buffer_mode;
    uint16_t pp_linue_num;
} cam_pp_param_t;

typedef struct {
    unsigned int width;
    unsigned int height;
} sensor_resolution_t;
#endif
