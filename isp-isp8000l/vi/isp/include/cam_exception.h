#ifndef __CAM_EXCEPTION_H__
#define __CAM_EXCEPTION_H__

#include "list.h"
#include "stdint.h"
#include "csi_camera.h"
#include "video.h"

typedef struct {
    char description[100];
    uint32_t sta_addr;
    uint64_t error_mask;
} cam_csi_err_data_t;

typedef enum cam_err_type{
    CAM_ERR_TYPE_SUBDEV_FATAL = 0,
    CAM_ERR_TYPE_NO_INPUT_DATA,
    CAM_ERR_TYPE_VIPPRE_FIFO_OVERFLOW,
    CAM_ERR_TYPE_VIPRE_BUS_ERROR,
    CAM_ERR_TYPE_NMOVERFLOW,
    CAM_ERR_TYPE_SENSOR_ERR,
    CAM_ERR_TYPE_CSI_ERR,
    CAM_ERR_TYPE_ISP_HW_DATA_LOSS,
    CAM_ERR_TYPE_ISP_HW_OVERFLOW,
    CAM_ERR_TYPE_ISP_HW_TIMEOUT,
    CAM_ERR_TYPE_ISP_HW_SIZE_ERROR,
    CAM_ERR_TYPE_ISP_HW_BUS_ERROR,
    CAM_ERR_TYPE_ISP_BUFFER_DROPPED_MCM_WR,
    CAM_ERR_TYPE_ISP_BUFFER_DROPPED_MP,
    CAM_ERR_TYPE_ISP_BUFFER_DROPPED_SP,
    CAM_ERR_TYPE_ISP_BUFFER_DROPPED_SP2,
    CAM_ERR_TYPE_ISP_BUFFER_DROPPED_PP,
    CAM_ERR_TYPE_RY_HW_DATA_LOSS,
    CAM_ERR_TYPE_RY_HW_TIMEOUT,
    CAM_ERR_TYPE_RY_HW_SIZE_ERROR,
    CAM_ERR_TYPE_RY_HW_BUS_ERROR,
    CAM_ERR_TYPE_RY_BUFFER_DROPPED_MP,
    CAM_ERR_TYPE_RY_BUFFER_DROPPED_SP,
    CAM_ERR_TYPE_RY_BUFFER_DROPPED_SP2,
    CAM_ERR_TYPE_DSP_PANIC,
    CAM_ERR_TYPE_DW_HW_TIMEOUT,
    CAM_ERR_TYPE_DW_IRQ_ERROR,
    CAM_ERR_TYPE_INVALID,
} cam_err_type_e;

typedef union{
    cam_csi_err_data_t csi_err;
} cam_err_data_t;

typedef struct {
    cam_err_type_e type;
    cam_err_data_t data;
} cam_error_t;

int cam_exception_handler(cam_device_t *cam_ctx ,int suddev_id, int path_id, cam_error_t *err);
#endif
