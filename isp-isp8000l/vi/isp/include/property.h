/*
 * Example application data structures.
 */

#ifndef PROPERTY_H
#define PROPERTY_H

#include <stdbool.h>
#include <stdint.h>
#include <csi_camera.h>

typedef struct csi_camera_property_description_list {
    struct csi_camera_property_description_list *next;
    csi_camera_property_description_s v;
} csi_camera_property_description_list_t;

typedef struct csi_camera_dev_property {
    struct csi_camera_dev_property *next;
    char *name;
   csi_camera_property_description_list_t *property;
} csi_camera_dev_property_t;

struct fruit {
    struct fruit *next;
    char *name;
    char *color;
    int count;
    struct variety *varieties;
};

struct variety {
    struct variety *next;
    char *name;
    char *color;
    bool seedless;
};

void bail(const char *msg);
void *bail_alloc(size_t size);
char *bail_strdup(const char *s);

void add_dev(csi_camera_dev_property_t **dlist, char *name, csi_camera_property_description_list_t *plist);
void add_property(csi_camera_property_description_list_t **plist, csi_camera_property_description_list_t *property);

void destroy_devs(csi_camera_dev_property_t **dlist);
void destroy_propertys(csi_camera_property_description_list_t **plist);

#endif
