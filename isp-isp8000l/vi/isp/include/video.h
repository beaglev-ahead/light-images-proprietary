/*
 * Copyright (C) 2017-2020 Alibaba Group Holding Limited
 */

/******************************************************************************
 * @file     video.h
 * @brief    Header File for video
 * @version  V1.0
 * @date     14. Apr 2022
 * @model    video
 ******************************************************************************/

#ifndef __THEAD_VIDEO_H__
#define __THEAD_VIDEO_H__

#include <bits/stdint-uintn.h>
#include <memory.h>
#include <sys/mman.h>
#include "list.h"
#include "stdint.h"
#include "property.h"
#include "subdev_shared_defs.h"
#include "syslog.h"
#include "cam_event.h"
#include "csi_camera.h"
#include "monitor_core.h"


#ifdef __cplusplus
extern "C" {
#endif

#define CHANNEL_NUM CSI_CAMERA_CHANNEL_MAX_COUNT
#define SUBDEV_NUM 16
#define PROPERTY_NUM 100
#define PATH_NUM_PER_SUBDEV 16
#define SENSOR_DETECT_FAIL -2
#define NUM_OF_BUFFERS  5
#define  NOTIFY_ALL_SUBDEV    0xFFFFBEEF
#define INVALID_ID  (0xFF)

struct cam_device;
struct cam_subdev;
struct cam_subdev_path;
struct cam_channel;

typedef struct {
    volatile uint64_t frame_irq_cnt;
    volatile uint64_t frame_time_us;
    volatile int floodlight_temperature;
    volatile int projection_temperature;
} frame_mark_info_t;

typedef enum {
    CAM_CHANNEL_INIT,
    CAM_CHANNEL_STANDY,
    CAM_CHANNEL_STREAM_ON,
    CAM_CHANNEL_STREAM_OFF,
    CAM_CHANNEL_STREAM_PAUSE,
} cam_channel_stat_e;

typedef enum {
    CAM_SUB_DEV_ERROR,
    CAM_SUB_DEV_FRAME_READY,
    CAM_SUB_DEV_OVERFLLOW,
} cam_subdev_event_e;
typedef enum{
    CAM_STATE_CHECK_FAIL =-1,
    CAM_STATE_CHECK_PASS =0,
    CAM_STATE_CHECK_REPEAT,

}cam_state_check_ret_e;
typedef enum{
    /*********camera level action***********/
    CAM_ACTION_CAM_MIN =0,
    CAM_ACTION_CAM_OPEN = CAM_ACTION_CAM_MIN,
    CAM_ACTION_CAM_CLOSE,
    CAM_ACTION_CAM_PROPERTY_SET,

    CAM_ACTION_CAM_CHANNEL_UPDATE,
    CAM_ACTION_CAM_EXCEPTION_RISE,
    CAM_ACTION_CAM_MAX = CAM_ACTION_CAM_EXCEPTION_RISE,
    /*********channel level action***********/
    CAM_ACTION_CHANNEL_MIN,
    CAM_ACTION_CHANNEL_OPEN = CAM_ACTION_CHANNEL_MIN,
    CAM_ACTION_CHANNEL_START,
    CAM_ACTION_CHANNEL_STOP,
    CAM_ACTION_CHANNEL_CLOSE,
    CAM_ACTION_CHANNEL_EXCEPTION_RISE,
    CAM_ACTION_CHANNEL_MAX = CAM_ACTION_CHANNEL_EXCEPTION_RISE,
}cam_action_type_e;
typedef enum {
    CAMERA_STATE_INVALID =0,
    CAMERA_STATE_UNINITIALIZATION ,
    CAMERA_STATE_INITIALIZATION,
    CAMERA_STATE_IDLE,
    CAMERA_STATE_RUNNING,
    CAMERA_STATE_EXCEPTION,
} camera_state_e;
typedef void (*subdev_callback_t)(void* context, cam_subdev_event_id_e event, void *arg,size_t size);

typedef enum subdev_sett_type {
   SUBDEV_SETT_TYPE_PATH_INIT,  //** path 的初始化,默认参数的配置
   SUBDEV_SETT_TYPE_PATH_INPUT_SET, //** 输入图片格式、地址发生变化
   SUBDEV_SETT_TYPE_PATH_OUTPUT_SET,
   SUBDEV_SETT_TYPE_PATH_ON,
   SUBDEV_SETT_TYPE_PATH_OFF,
   SUBDEV_SETT_TYPE_PATH_PAUSE,
   SUBDEV_SETT_TYPE_CHANNEL_OUTPUT_SET,
   SUBDEV_SETT_TYPE_PATH_DSP_ALGO_PARAM_SET,
   SUBDEV_SETT_TYPE_PATH_DSP_ALGO_SETTING_UPDATE,
   SUBDEV_SETT_TYPE_PATH_DSP_ALGO_BUF_UPDATE,
   SUBDEV_SETT_TYPE_PATH_PP_PARAM_SET,
   SUBDEV_SETT_TYPE_PATH_ISP_AE_BUF_SET,
   SUBDEV_SETT_TYPE_PATH_FLASH_LED_PARAM_SET,
   SUBDEV_SETT_TYPE_PATH_CHECK_STATUS,
   SUBDEV_SETT_TYPE_PATH_EXCEPRTION_PROCESS,

/*********cmd with param  set  by set_param(path,cmd,param)***********************/
   SUBDEV_SETT_TYPE_PROPERTY_SET,
   SUBDEV_SETT_TYPE_PROPERTY_GET,
   SUBDEV_SETT_TYPE_SEND_FRAME,
   SUBDEV_SETT_TYPE_SENSOR_RESOLUTION_SET,
   SUBDEV_SETT_TYPE_TEST_ENABLE,
   SUBDEV_SETT_TYPE_SENSOR_ENV,
   SUBDEV_SETT_TYPE_SRAM_POOL,
}subdev_sett_type_t;

typedef enum subdev_params_type{

   SUBDEV_PARAMS_TYPE_PATH_OUT,      // Path 输出参数
   SUBDEV_PARAMS_TYPE_PP_PATH_OUT,
   SUBDEV_PARAMS_TYPE_CHANNEL_OUT,   // Path 输出参数
   SUBDEV_PARAMS_TYPE_PROPERTY_1,
   SUBDEV_PARAMS_TYPE_SENSOR_NAME,
}subdev_params_type_t;

struct cam_allocation_ops {
	uint64_t (*alloc)(struct cam_allocation_pool *allocation_pool, uint32_t size, uint32_t align);
	void (*free)(struct cam_allocation_pool *allocation_pool,uint64_t addr,uint32_t size);
	void (*free_pool)(struct xrp_allocation_pool *allocation_pool);

};
struct cam_allocation_pool {
	const struct cam_allocation_ops *ops;
};

typedef struct cam_subdev {
    char dev_name[40];
    int idx;                       //index for device
    int fd;
    void *priv_data;
    int subdev_idx;                  //index in video's subdev
    void *subdev_path[PATH_NUM_PER_SUBDEV];
    int  path_num;
    int (*create)(struct cam_subdev *dev);
    int (*destroy)(struct cam_subdev *dev);
    int (*update)(struct cam_subdev_path *path, void *obj);
    int (*set_param)(struct cam_subdev_path *path,subdev_sett_type_t type,void *arg); //接口提供给 CSI CAM、channel 设置path的参数和property.
    int (*get_param)(struct cam_subdev_path *path,subdev_params_type_t type, void *arg);// 接口提供subdev、cam、channel 参数 相关状态和参数。
    int (*check_status)(struct cam_subdev_path *path, void *arg); //用于monitor 检查设备状态.
    int (*exceprtion_process)(struct cam_subdev_path *path, void *arg); //用于monitor 检查到设备处理异常.
    int vivcamID;
    struct cam_device *cam;
} cam_subdev_t;

typedef struct cam_subdev_path {
    struct list_head observers;
    subdev_callback_t callback;
    void *callback_arg;
    cam_subdev_t *sub_dev;
    int path_type;
    int path_index;
	int chl_idx;
	int channel_id;
} cam_subdev_path_t;

typedef enum {
    CAM_VGA,
    CAM_1080P,
} cam_resolution_e;

typedef struct {
    int h_pixel;
    int v_pixel;
    int bayer_mode;
    csi_pixel_fmt_e pix_fmt;
    int fps;
    char calibFileName[60];
} cam_img_param_t;

typedef struct {
    cam_img_param_t img_info;
    int exp_time;
    int exp_again;
    int exp_dgain;
    int stream_on;
    int channel_id;
} cam_channel_param_t;

 typedef enum {
     VI_CAMERA_CHANNEL_EVENT_FRAME_READY, // Channel的帧已经准备好，可以取出
     VI_CAMERA_CHANNEL_EVENT_FRAME_PUT,   // 取出的帧被返还给channel
     VI_CAMERA_CHANNEL_EVENT_OVERFLOW,    // Channel的帧缓冲已满
 } vi_camera_channel_event_id_e;

typedef void (*channel_callback_t)(struct cam_channel *ch, csi_camera_channel_event_id_e event, void *arg);

typedef struct {
    struct list_head head;
    unsigned long frame_num;
    pthread_mutex_t mutex;
    pthread_cond_t cond;
} vi_frames_t;

typedef struct {
    struct list_head entry;
    void *data;
} vi_frames_item_t;

typedef struct cam_channel {
    struct list_head observers;
    channel_callback_t callback;
    void *callback_arg;
    cam_channel_param_t param;
    csi_pipeline_event_ctx_t *ev_ctx;
    vi_frames_t  channel_frame;
    int channel_id;
	int frame_id;
	char sensor_name[32];
	unsigned int meta_num;
    int frame_info_map_fd;
    frame_mark_info_t *frame_info;
    csi_camera_channel_cfg_s cfg;
    int (*attach)(struct cam_channel *ch, void *obj); //注册观察者
    int (*dattach)(struct cam_channel *ch, void *obj);//删除观察者
    int (*set_param)(struct cam_channel *ch, cam_channel_param_t *param);
    int (*get_param)(struct cam_channel *ch, cam_channel_param_t *param);
    int (*check_status)(struct cam_channel *ch);
    int (*exceprtion_process)(struct cam_channel *ch);
    int (*attach_callback)(struct cam_channel *ch, channel_callback_t callback, void *arg);
    int (*dattach_callback)(struct cam_channel *ch);
    bool loopback;
} cam_channel_t;

typedef struct {
    char name[40];
    int path;
    int idx;
    int param_size;
    char param[400];
} sub_dev_info_t;

typedef struct {
    int sub_dev_num;
    sub_dev_info_t sub_dev[20];
} channel_t;

typedef struct {
    int channel_num;
    channel_t channels[20];
} channel_cfg_t;


typedef struct cam_device {
    int fd;
    camera_state_e  state;
    int channel_num;
    cam_channel_t channel[CHANNEL_NUM];
    int subdev_num;
    cam_subdev_t *subdevs[SUBDEV_NUM];      //存储所有已经存在的实例
    csi_camera_dev_property_t *propertys;   //存储设配属性
    csi_cam_event_ctx_t *ev_ctx;
    channel_cfg_t chcfg;
    int sensor_fps;
    int stream_on_for_pause; // 对应pause bit位图引用计数，记录已起流的channel，每个bit代表一个channel
    int stream_on_for_off; // 对应stream off 的bit位图引用计数，记录已起流的channel，每个bit代表一个channel
    int mounted_sensor;
    struct cam_allocation_pool  *sram_pool;
    int vi_mem_pool_region_id;
    vi_monitor_handle_t vim_handle;
    char name[40];
} cam_device_t;

typedef struct {
	uint8_t *pYBase, *pCbCrBase;
	uint32_t YCPlaneSize;
	uint32_t CbcRCPlaneSize;
} isp_frame_t;

typedef  struct {
    unsigned int frame_cnt;
    uint64_t dstBufferAddr[30];
    uint32_t dstBufferSize[30];
} camera_frame_t;

typedef struct {
    char* subdev_name;
    char* sensor_name;
    unsigned int path_type;
    unsigned int pipeline_id;
} pathtype_set_t;


static inline long cam_allocate(void*allocate_pool,uint32_t size, uint32_t align)
{
    return ((struct cam_allocation_pool *)allocate_pool)->ops->alloc((struct cam_allocation_pool*)allocate_pool, size, align);
}

static inline void cam_free(void*allocate_pool,uint64_t addr, uint32_t size)
{
    ((struct cam_allocation_pool *)allocate_pool)->ops->free((struct cam_allocation_pool*)allocate_pool, addr, size);
}
/**
 * @description: Scan all existing video devices and
 * generate related video device information description
 * @param {cam_infos} Storage video device information
 * @return {int} 0 success, other error code
 */
int cam_scan_devices(csi_camera_infos_s *cam_infos);

/**
 * @description: Open a video device
 * @param {cam}  Video handle to operate
 * @param {name} The video devide name
 * @return {int} 0 success, other error code
 */
int cam_open(cam_device_t *cam, char *name);

/**
 * @description: Close a video device
 * @param {cam}  Video handle to operate
 * @return {int} 0 success, other error code
 */
int cam_close(cam_device_t *cam);

/**
 * @description: Register an observer for a sub-device
 * @param {path} Output path of a sub-device
 * @param {obj}  An observer for sub-devide
 * @return {int} 0 success, other error code
 */
int cam_subdev_attach(cam_subdev_path_t *path, void *obj);

/**
 * @description: Unregister an observer for a sub-device
 * @param {path} Output path of a sub-device
 * @param {obj}  An observer for sub-devide
 * @return {int} 0 success, other error code
 */
int cam_subdev_dattach(cam_subdev_path_t *path, void *obj);

/**
 * @description: The observer to which the child device publishes the message
 * @param {path} Output path of a sub-device
 * @param {type} Message type
 * @param {arg}  Message param
 * @param {channel_id} The video channel it is in
 * @return {int} 0 success, other error code
 */
int cam_subdev_notify(cam_subdev_path_t *path,subdev_sett_type_t type, void* arg, int channel_id);


bool cam_channel_refcnt_check(cam_device_t *cam, int chn_id, cam_channel_stat_e on);

/**
 * @description: Register the callback function with the output channel of the sub-device
 * @param {path} Output path of a sub-device
 * @param {callback} Callback
 * @param {arg}  Callback param
 * @return {int} 0 success, other error code
 */
int cam_subdev_attach_callback(struct cam_subdev_path *path, subdev_callback_t callback, void *arg);

/**
 * @description: Unregister the callback function with the output channel of the sub-device
 * @param {path} Output path of a sub-device
 * @return {int} 0 success, other error code
 */
int cam_subdev_dattach_callback(struct cam_subdev_path *path);

/**
 * @description: Create sub-device
 * @param {cam} A pointer to cam_device
 * @param {subdev} A pointer used storage sub-device handle
 * @param {dev_name} Sub-device name
 * @param {dev_idx} Sub-device idx
 * @return {int} 0 success, other error code
 */
int cam_create_subdev(cam_device_t *cam, cam_subdev_t **subdev, char *dev_name, int dev_idx,int sensor);

/**
 * @description: Destory sub-device
 * @param {subdev} Sub-device handle
 * @return {int} 0 success, other error code
 */
int cam_destroy_subdev(cam_subdev_t *subdev);

/**
 * @description: Create sub-device output path
 * @param {path_type} The Sub-device output path type
 * @param {subdev} Sub-device handle
 * @return {pointer} success:sub-device output path handle, error: NULL
 */
cam_subdev_path_t *cam_subdev_path_create(int path_type, cam_subdev_t *sub_dev);

/**
 * @description: Destory sub-device output path
 * @param {path} sub-device output path handle
 * @return {int} 0 success, other error code
 */
int cam_subdev_path_destroy(cam_subdev_path_t *path);

/**
 * @description: Init the video channel
 * @param {ch} channel handle
 * @param {cam} video handle
 * @param {ch_id} channel id
 * @return {int} 0 success, other error code
 */
int cam_channel_init(cam_channel_t *ch, cam_device_t *cam, int ch_id);

/**
 * @description: Uninit the video channel
 * @param {cam} video handle
 * @param {ch} channel handle
 * @return {int} 0 success, other error code
 */
int cam_channel_uninit(cam_device_t *cam, struct cam_channel *ch);



/**
 * @description: Put a frame into the video channel
 * @param {ch}   Channel handle
 * @param {cam_frame} Image frame
 * @return {int} 0 success, other error code
 */
int cam_channel_frame_push(struct cam_channel *ch , cam_frame_img_t *cam_frame);

/**
 * @description: Pop a frame from video channel
 * @param {ch}   Channel handle
 * @param {cam_frame} Image frame
 * @return {int} 0 success, other error code
 */
int cam_channel_frame_pop(struct cam_channel *ch, csi_frame_ex_s *frame,int timeout_ms);

/**
 * @description: Free the image frame
 * @param {cam_frame} Image frame
 * @return {int} 0 success, other error code
 */
int cam_free_frame_buf(csi_frame_ex_s *frame);

/**
 * @description: Find the sub-device corresponding to the
 * property function through the property id
 * @param {cam} Video handle
 * @param {pid} Property id
 * @return {pointer} success: sub-device handle, error: NULL
 */
cam_subdev_t *cam_property_id_to_dev(cam_device_t *cam, int pid);

/**
 * @description: Find the corresponding property description by property id
 * @param {cam} Video handle
 * @param {pid} Property id
 * @return {int} 0 success, other error code
 */
csi_camera_property_description_s* cam_find_property_description(cam_device_t *cam, int pid);

/**
 * @description: Find the output path of a subdevice
 * @param {dev}: sub-device hanele
 * @param {name}: sub-device name
 * @param {idx}: sub-device idx
 * @param {paht_type}: sub-device output paht type
 * @return {pointer} success: sub-device path handle, error NULL
 */
int cam_manager_sram_int(cam_device_t *cam,uint64_t start_addr,uint32_t size);
cam_subdev_path_t *cam_find_path(cam_device_t *dev, char *name, int idx, int path_type);

int cam_state_check_for_property(cam_device_t *dev,uint32_t propery_id);

struct timespec cam_tm_add_ms(struct timespec tm,int time_ms);
#ifdef __cplusplus
}
#endif

#endif
