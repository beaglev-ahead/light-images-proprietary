#ifndef CAM_PROPERTY_PARSE_H
#define CAM_PROPERTY_PARSE_H

#include "property.h"
#include "video.h"

/* Our example parser states. */
enum state {
    STATE_START,    /* start state */
    STATE_STREAM,   /* start/end stream */
    STATE_DOCUMENT, /* start/end document */
    STATE_SECTION,  /* top level */

    STATE_FLIST,    /* dev list */
    STATE_FVALUES,  /* dev key-value pairs */
    STATE_FKEY,     /* dev key */
    STATE_FNAME,    /* dev name value */

    STATE_VLIST,    /* propertys list */
    STATE_VVALUES,  /* property key-value pairs */
    STATE_VKEY,     /* property key  */
    STATE_VNAME,    /* property name */
    STATE_VPID,     /* property pid  */
    STATE_VTYPE,    /* property type */
    STATE_VMIN,     /* property minimum */
    STATE_VMAX,     /* property maximum */
    STATE_VSTEP,    /* property step    */
    STATE_VDEF,     /* property default val */
    STATE_VFLAG,    /* property flag   */
    STATE_VENABLE,  /* property enable */
    STATE_STOP      /* end state       */
};

/* Our application parser state data. */
struct parser_state {
    enum state state;      /* The current parse state */
    csi_camera_dev_property_t d;
    csi_camera_property_description_list_t p;
    csi_camera_property_description_list_t *plist;
    csi_camera_dev_property_t *dlist;
};

/**
 * @description: display property of all subdevs as a table
 * @param {dlist} csi_camera_dev_property_t property list
 * @return none
 */
void cam_property_show_table(csi_camera_dev_property_t *dlist);

/**
 * @description: config property of all subdevs
 * @param {cam} cam_device_t
 * @return {int} 0 success, other error code
 */
int cam_property_config(cam_device_t *cam);

/**
 * @description: destroy property of all subdevs
 * @param {cam} cam_device_t
 * @return {int} 0 success, other error code
 */
int cam_property_destroy(cam_device_t *cam);

/**
 * @description: find the corresponding cam_subdev_t to the specific property id
 * @param {cam} cam_device_t
 * @param {pid} property id
 * @return {cam_subdev_t} subdev pointer
 */
cam_subdev_t *cam_property_id_to_dev(cam_device_t *cam, int pid);

/**
 * @description: find the corresponding description to the specific property id
 * @param {cam} cam_device_t
 * @param {pid} property id
 * @return {csi_camera_property_description_s} description pointer
 */
csi_camera_property_description_s* cam_property_find_description(cam_device_t *cam, int pid);

/**
 * @description: query the corresponding description to the specific property id
 * @param {cam} cam_device_t
 * @param {dest_id} property id
 * @return {csi_camera_property_description_s} description pointer
 */
csi_camera_property_description_s* cam_property_query(cam_device_t *cam_device, unsigned int dest_id);
#endif
