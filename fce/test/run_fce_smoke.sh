#!/bin/sh
BASE_PATH=./
TEST_PATH=${BASE_PATH}test
RES_PATH=${TEST_PATH}/resource/
OUT_PATH=${TEST_PATH}/out

#export LD_LIBRARY_PATH=${BASE_PATH}/lib/:$LD_LIBRARY_PATH

VERBOSE=0
BASENUM=100000
LOOP=1
JOBS=1
while getopts ":v:b:l:j:" opt
do
  case $opt in
    v)
      VERBOSE=$OPTARG
    ;;
    b)
      BASENUM=$OPTARG
    ;;
    l)
      LOOP=$OPTARG
    ;;
    j)
      JOBS=$OPTARG
    ;;
    ?)
      echo "unsupport parameter!!!"
      exit 1;;
  esac
done

echo "BASENUM="${BASENUM}", LOOP="${LOOP}", JOBS="${JOBS}

rm -rf ${OUT_PATH}; mkdir -p ${OUT_PATH}

rm -f test.log
JOB=0
while [ ${JOB} -lt ${JOBS} ]
do
  if [ "${VERBOSE}" == "1" ]; then
    ${TEST_PATH}/fce_smoke ${BASENUM} ${LOOP} &
  else
    ${TEST_PATH}/fce_smoke ${BASENUM} ${LOOP} >> test.log &
  fi
  JOB=$((JOB+1))
done
